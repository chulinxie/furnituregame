﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class HandCtrl : MonoBehaviour
{
    public GameObject Touchhighlightpoiont;
    public GameObject Movehighlightpoint;
    public GameObject Walkhighlightpoint;
    public GameObject hero_controller;
    public GameObject CanvasObj;
    private GameObject scanned_touchable_obj;

    // Start is called before the first frame update
    void Start()
    {
        scanned_touchable_obj = null;
    }

    // Update is called once per frame
    void Update()
    {
        MakeHighlight();
        CheckWalk();
        CheckChosenObj();
        CheckMenu();
    }

    void CheckMenu()
    {
        if (Input.GetKey(KeyCode.Alpha0))// 进入选择menu
        {
            Global.state = Global.WATCH_MENU;
            CanvasObj.GetComponent<UIMenu>().OnMenuEnter();
            return;
        }
        //UIMenu中会处理 WATCH_MENU REMOVE_OBJ
        //UILib CHANGE_MATERIAL  ADD_OBJ  REPLACE_OBJ   
        if (Global.state == Global.ROTATE_OBJ)
        {
            if (Global.chosenObj == null)
            {
                CanvasObj.GetComponent<UICtrl>().UICtrlMsgSetString("请先选择物体！");
                return;
            }
            if (Input.GetKey(KeyCode.LeftArrow)) 
            { 
               Global.chosenObj.transform.Rotate(new Vector3(0, 1 * Time.deltaTime * Global.rotate_speed, 0));
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {

                Global.chosenObj.transform.Rotate(new Vector3(0, 1 * Time.deltaTime * Global.rotate_speed, 0));
            }
            if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                DisChoose();
                Global.state = Global.IDLE;
            }
            if (Input.GetKeyUp(KeyCode.RightArrow))
            {

                DisChoose();
                Global.state = Global.IDLE;
            }
        }

    }

    void CheckWalk()
    {
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            // 点击UI时不触发场景物体的响应
            if (EventSystem.current.IsPointerOverGameObject())
            {
                Debug.Log("touch area is UI");
                return;
            }

            Ray ray = new Ray(transform.position, transform.forward * 100);     //定义一个射线对象,包含射线发射的位置transform.position，发射距离transform.forward*100；  
            RaycastHit hitInfo = new RaycastHit();    //定义一个RaycastHit变量用来保存被撞物体的信息；  
            if (Physics.Raycast(ray, out hitInfo, 100))  //如果碰撞到了物体，hitInfo里面就包含该物体的相关信息；  
            {
                Debug.Log("hitinfo: " + hitInfo.collider.gameObject.name);
                if (hitInfo.collider.gameObject.tag.Equals("Walkable"))
                {
                    if(Global.state == Global.MOVE_OBJ)
                    {
                        if (Global.chosenObj == null)
                        {
                            GetComponent<UICtrl>().UICtrlMsgSetString("请先选择物体！");
                            return;
                        }

                        Global.chosenObj.transform.position = new Vector3(hitInfo.point.x, 
                                                                Global.chosenObj.transform.position.y,
                                                                    hitInfo.point.z);
                        DisChoose();
                        Global.state = Global.IDLE;
                    }
                    else {
                        print(" will move to a  Walkable");
                        if (Vector3.Distance(hitInfo.point, hero_controller.transform.position) < 10f
                                && Mathf.Abs(hitInfo.point[1] - hero_controller.transform.position[1]) < 10f)
                        {
                            HeroMove.target = hitInfo.point;
                            HeroMove.target.y = hero_controller.transform.position.y;
                            HeroMove.isMoveOver = false;
                            //2. 让角色移动到目标位置
                            HeroMove.source = hero_controller.transform.position;
                        }
                    }

                }

            }
        }
    }

    void CheckChosenObj()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (Global.chosenObj != null) {
                //取消选中物体，恢复到idle状态
                DisChoose();
                Global.state = Global.IDLE;
            }
            else 
            {
                Ray ray = new Ray(transform.position, transform.forward * 100);     //定义一个射线对象,包含射线发射的位置transform.position，发射距离transform.forward*100；  
                RaycastHit hitInfo = new RaycastHit();    //定义一个RaycastHit变量用来保存被撞物体的信息；  
                if (Physics.Raycast(ray, out hitInfo, 100))  //如果碰撞到了物体，hitInfo里面就包含该物体的相关信息；  
                {
                    Debug.Log("hitinfo: " + hitInfo.collider.gameObject.name);
                    if (hitInfo.collider.gameObject.tag.Equals("Touchable"))
                    {
                        if (Vector3.Distance(hitInfo.point, hero_controller.transform.position) < 10f
                                && Mathf.Abs(hitInfo.point[1] - hero_controller.transform.position[1]) < 10f)
                        {
                            Debug.Log(" chonsen obj!! it's within the distance");
                            ChooseObject(hitInfo.collider.gameObject);
                        }
                    }
                }
            }
        }
    }

    public static void ChooseObject(GameObject obj)
    {
        Global.chosenObj = obj;
        Global.chosenObj.GetComponent<ObjShowRim>().OnSelectEnter();
    }

    public static void DisChoose()
    {
        if (Global.chosenObj == null) return;
        Global.chosenObj.GetComponent<ObjShowRim>().OnSelectExit();
        Global.chosenObj = null;
    }

    void MakeHighlight()
    {
        Ray ray = new Ray(transform.position, transform.forward * 100);          //定义一个射线对象,包含射线发射的位置transform.position，发射距离transform.forward*100；  
        Debug.DrawLine(transform.position, transform.position + transform.forward * 100, Color.red);  //绘制出的射线，包含发射位置，发射距离和射线的颜色；  
        RaycastHit hitInfo;                                 //定义一个RaycastHit变量用来保存被撞物体的信息；  
        if (Physics.Raycast(ray, out hitInfo, 100))         //如果碰撞到了物体，hitInfo里面就包含该物体的相关信息；  
        {
            Debug.Log("hitinfo: " + hitInfo.collider.gameObject.name + hitInfo.collider.gameObject.tag);
            if (hitInfo.collider.gameObject.tag.Equals("Touchable"))
            {
                if (scanned_touchable_obj == null) {
                    scanned_touchable_obj = hitInfo.collider.gameObject;
                    scanned_touchable_obj.GetComponent<ObjShowRim>().OnScanEnter();
                } else if (scanned_touchable_obj != hitInfo.collider.gameObject) {
                    // scan到和上次不一样的物体
                    scanned_touchable_obj.GetComponent<ObjShowRim>().OnScanExit();
                    scanned_touchable_obj = hitInfo.collider.gameObject;
                    scanned_touchable_obj.GetComponent<ObjShowRim>().OnScanEnter();
                }

                //Debug.Log("hit Touchable");
                Touchhighlightpoiont.transform.position = hitInfo.point;
                Touchhighlightpoiont.GetComponent<MeshRenderer>().enabled = true;
                Walkhighlightpoint.GetComponent<MeshRenderer>().enabled = false;
                Movehighlightpoint.GetComponent<MeshRenderer>().enabled = false;
            }
            else if (hitInfo.collider.gameObject.tag.Equals("Walkable"))
            {
                //Debug.Log("hit Walkable");
                if (Global.state == Global.MOVE_OBJ)
                {
                    Movehighlightpoint.transform.position = hitInfo.point;
                    Movehighlightpoint.GetComponent<MeshRenderer>().enabled = true;
                    Walkhighlightpoint.GetComponent<MeshRenderer>().enabled = false;
                }
                else
                {
                    Walkhighlightpoint.transform.position = hitInfo.point;
                    Walkhighlightpoint.GetComponent<MeshRenderer>().enabled = true;
                    Movehighlightpoint.GetComponent<MeshRenderer>().enabled = false;
                }
                if (scanned_touchable_obj != null)
                {
                    scanned_touchable_obj.GetComponent<ObjShowRim>().OnScanExit();
                    scanned_touchable_obj = null;
                }
                Touchhighlightpoiont.GetComponent<MeshRenderer>().enabled = false;

            }
            else
            {
                NoHighlightPoint();
            }
        }
        else
        {
            NoHighlightPoint();
        }
    }

    private void NoHighlightPoint()
    {
        if (scanned_touchable_obj != null)
        {
            scanned_touchable_obj.GetComponent<ObjShowRim>().OnScanExit();
            scanned_touchable_obj = null;
        }
        Walkhighlightpoint.GetComponent<MeshRenderer>().enabled = false;
        Touchhighlightpoiont.GetComponent<MeshRenderer>().enabled = false;
        Movehighlightpoint.GetComponent<MeshRenderer>().enabled = false;
    }



}
