﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global
{
    // define the states
    public const int IDLE = 0;
   
    public const int MOVE_OBJ = 1; //选择 移动的位置
    public const int REPLACE_OBJ = 2; //选择 更改的物体
    public const int CHANGE_MATERIAL = 3; //选择 替换材质
    public const int ROTATE_OBJ = 4;  //选择 旋转的角度
    public const int ADD_OBJ = 5; //选择 被添加的物体
    public const int REMOVE_OBJ = 6; //没有中间状态，其实不需要有这个。。
    public const int WATCH_MENU = 7; //正在选择菜单 

    public const float  rotate_speed = 50f;
    //public const int 

    public static int state = IDLE;
 
    public static GameObject chosenObj = null;

    public static string StateStr()
    {
        switch (state)
        {
            case 0:
                return "IDLE";
            case 1:
                return "MOVE_OBJ";
            case 2:
                return "REPLACE_OBJ";
            case 3:
                return "CHANGE_MATERIAL";
            case 4:
                return "ROTATE_OBJ";
            case 5:
                return "ADD_OBJ";
            case 6:
                return "REMOVE_OBJ";
            case 7:
                return "WATCH_MENU";
            default:
                return "";
               
        }
    }

}


public class FollowCtrl : MonoBehaviour
{
    // Use this for initialization
    public GameObject controller;
    void Start()
    {
        transform.position = controller.transform.position;
        transform.rotation = controller.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = controller.transform.position;
        transform.rotation = controller.transform.rotation;
        //Debug.Log("hero state : " + Global.state);
        //if (Global.state != Global.CLIMB_L || Global.state != Global.CLIMB_R)
        //{
        //    Global.touchThing = null;
        //}

        //if (Global.state == Global.IDLE)
        //{
        //    controller.GetComponent<Rigidbody>().isKinematic = false;
        //}
    }
}
