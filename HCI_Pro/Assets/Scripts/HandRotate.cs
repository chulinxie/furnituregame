﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandRotate : MonoBehaviour
{
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.I))
        {
            transform.RotateAround(transform.position, new Vector3(0, 0, 1), 60f * Time.deltaTime);//第三个参数表示角度
        }
        if (Input.GetKey(KeyCode.K))
        {
            transform.RotateAround(transform.position, new Vector3(0, 0, -1), 60f * Time.deltaTime);//第三个参数表示角度
        }

        if (Input.GetKey(KeyCode.L))
        {
            transform.RotateAround(transform.position, new Vector3(0, 1, 0), 60f * Time.deltaTime);//第三个参数表示角度
        }
        if (Input.GetKey(KeyCode.J))
        {
            transform.RotateAround(transform.position, new Vector3(0, -1, 0), 60f * Time.deltaTime);//第三个参数表示角度
        }
        if (Input.GetKey(KeyCode.N))
        {
            transform.RotateAround(transform.position, new Vector3(-1, 0, 0), 60f * Time.deltaTime);//第三个参数表示角度
        }
        if (Input.GetKey(KeyCode.M))
        {
            transform.RotateAround(transform.position, new Vector3(1, 0, 0), 60f * Time.deltaTime);//第三个参数表示角度
        }

    }
}
