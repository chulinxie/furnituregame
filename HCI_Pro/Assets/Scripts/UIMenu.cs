﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMenu : MonoBehaviour
{
    public GameObject button_menu;
    GameObject replace_obj_btn;
    GameObject remove_obj_btn;
    GameObject add_obj_btn;
    GameObject rotate_obj_btn;
    GameObject move_obj_btn;
    GameObject change_mat_btn;
    GameObject close_menu_btn;
    // Start is called before the first frame update
    void Start()
    {
        BindUIs();
    }

    void BindUIs()
    {
        replace_obj_btn = button_menu.transform.Find("replace_obj_button").gameObject;
        remove_obj_btn = button_menu.transform.Find("remove_obj_button").gameObject;
        add_obj_btn = button_menu.transform.Find("add_obj_button").gameObject;
        rotate_obj_btn = button_menu.transform.Find("rotate_obj_button").gameObject;
        move_obj_btn = button_menu.transform.Find("move_obj_button").gameObject;
        change_mat_btn = button_menu.transform.Find("change_mat_button").gameObject;
        close_menu_btn = button_menu.transform.Find("close_menu_button").gameObject;

        replace_obj_btn.GetComponent<Button>().onClick.AddListener(ReplaceOnClick);
        remove_obj_btn.GetComponent<Button>().onClick.AddListener(RemoveOnClick);
        add_obj_btn.GetComponent<Button>().onClick.AddListener(AddOnClick);
        rotate_obj_btn.GetComponent<Button>().onClick.AddListener(RotateOnClick);
        move_obj_btn.GetComponent<Button>().onClick.AddListener(MoveOnClick);
        change_mat_btn.GetComponent<Button>().onClick.AddListener(ChangeMatOnClick);
        close_menu_btn.GetComponent<Button>().onClick.AddListener(CloseMenuOnClick);
        button_menu.SetActive(false);//隐藏不显示
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnMenuEnter()
    {
        button_menu.SetActive(true);
    }

    public void OnMenuExit()
    {
        button_menu.SetActive(false);

    }

    void ReplaceOnClick()
    {
        if (Global.chosenObj == null)
        {
            MsgSetString("请先选择物体！");
            return;
        }
        Global.state = Global.REPLACE_OBJ;
        MsgSetString("请选择作为替换的物体");
        OnMenuExit();
        // 开启library ui菜单，选择replace的obj
        GetComponent<UILib>().OnObjLibEnter();
    }

    void MoveOnClick()
    {
        if (Global.chosenObj == null)
        {
            MsgSetString("请先选择物体！");
            return;
        }
        Global.state = Global.MOVE_OBJ;
        MsgSetString("请选择移动的位置");
        OnMenuExit();
        // 进入选择位置的阶段
    }
    void AddOnClick()
    {
        Global.state = Global.ADD_OBJ;
        MsgSetString("请选择要添加的物体");
        OnMenuExit();
        GetComponent<UILib>().OnObjLibEnter();
        // 开启ui菜单，选择add的obj
    }

    void RemoveOnClick()
    {
        if (Global.chosenObj == null)
        {
            MsgSetString("请先选择物体！");
            return;
        }
        Global.chosenObj.SetActive(false);
        Global.chosenObj = null;
        OnMenuExit();
        Global.state = Global.IDLE;
    }
    void RotateOnClick()
    {
        if (Global.chosenObj == null)
        {
            MsgSetString("请先选择物体！");
            return;
        }

        MsgSetString("请按 左右方向键 选择角度");
        OnMenuExit();
        Global.state = Global.ROTATE_OBJ;
        // 进入选择角度的状态

    }
    void ChangeMatOnClick()
    {
        if (Global.chosenObj == null)
        {
            MsgSetString("请先选择物体！");
            return;
        }
        Global.state = Global.CHANGE_MATERIAL;
        MsgSetString("请选择作为替换的材质");
        OnMenuExit();
        GetComponent<UILib>().OnMatLibEnter();
        // 开启ui菜单，选择替换的mat;

    }

    void CloseMenuOnClick()
    {
        OnMenuExit();
        Global.state = Global.IDLE;
    }

    private void MsgSetString(string str)
    {
        this.GetComponent<UICtrl>().UICtrlMsgSetString(str);
  
    }
}
