﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UICtrl : MonoBehaviour
{
    public Text chosen_obj_text;
    public Text msg_text;
    public Text state;

    // Start is called before the first frame update
    void Start()
    {
        BindUIs();
    }

    void BindUIs()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

        if (Global.chosenObj == null)
        {
            chosen_obj_text.text = "chosen obj: none";
        }
        else
        {
            chosen_obj_text.text = "chosen obj:" + Global.chosenObj.name;
        }

        state.text = "Player State: " + Global.StateStr();

    }

    public void UICtrlMsgSetString(string str)
    {
        StartCoroutine(DisplayMsg(str));
    }

    private IEnumerator DisplayMsg(string str)
    {
        msg_text.text = str;
        yield return new WaitForSeconds(2); //等待2秒后清空消息
        msg_text.text = "";
    }

}