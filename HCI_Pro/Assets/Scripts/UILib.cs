﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UILib : MonoBehaviour
{
    public GameObject objs_lib_ui;
    public GameObject mats_lib_ui;

    GameObject objs_return_menu_btn;
    GameObject mats_return_menu_btn;
    GameObject objs_close_btn;
    GameObject mats_close_btn;

    List<GameObject> objs_btns=new List<GameObject>();
    List<GameObject> mats_btns= new List<GameObject>();
    List<Object> objs_prefabs= new List<Object>();
    List<Object> mats_prefabs= new List<Object>();
    string[] objs_prefabs_names = { "Chair1", "Chair2", "Sofa1", "Sofa2", "Table1"};

    private Vector3 born_pos = Vector3.zero;
    private Quaternion born_rot = new Quaternion(0, 0, 0, 0);
    // Start is called before the first frame update
    void Start()
    {
        BindUIs();
        objs_lib_ui.SetActive(false);
        mats_lib_ui.SetActive(false);

    }
    void BindUIs()
    {
        objs_return_menu_btn = objs_lib_ui.transform.Find("return_button").gameObject;
        mats_return_menu_btn = mats_lib_ui.transform.Find("return_button").gameObject;
        objs_return_menu_btn.GetComponent<Button>().onClick.AddListener(delegate ()
        {
            ReturnToMenuOnClick(0);
        });
        mats_return_menu_btn.GetComponent<Button>().onClick.AddListener(delegate ()
        {
            ReturnToMenuOnClick(1);
        });

        objs_close_btn = objs_lib_ui.transform.Find("close_button").gameObject;
        mats_close_btn = mats_lib_ui.transform.Find("close_button").gameObject;
        objs_close_btn.GetComponent<Button>().onClick.AddListener(delegate ()
        {
            CloseOnClick(0);
        });
        mats_close_btn.GetComponent<Button>().onClick.AddListener(delegate ()
        {
            CloseOnClick(1);
        });

        int index = 0;
        Debug.Log(objs_lib_ui.transform.Find("lib_btns").gameObject.name);
        foreach (Transform child in objs_lib_ui.transform.Find("lib_btns").gameObject.transform)
        {

            objs_btns.Add(child.gameObject);
            int tempindex = index; //传入addlistener前必须重新定一个变量。不能用临时变量index
            Debug.Log("bind index" + tempindex);
            child.gameObject.GetComponent<Button>().onClick.AddListener(delegate ()
            {
                ObjBtnOnClick(tempindex);
            });
            //Debug.Log(child.gameObject.name);
            index += 1;
        }
        index = 0;
        foreach (Transform child in mats_lib_ui.transform.Find("lib_btns").gameObject.transform)
        {
            mats_btns.Add(child.gameObject);
            int tempindex = index;
            child.gameObject.GetComponent<Button>().onClick.AddListener(delegate ()
            {
                MatBtnOnClick(index);
            });
            //Debug.Log(child.gameObject.name);
            index += 1;
        }

        // load furniture object prefabs
        for (int i = 0; i < objs_prefabs_names.Length; i++)
        {
            string path = "Prefabs/" + objs_prefabs_names[i];
            objs_prefabs.Add(Resources.Load(path, typeof(GameObject)));
        }

        //TODO Load mats_prefabs
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ObjBtnOnClick(int index)
    {
        Debug.Log("ObjBtnOnClick index" + index);
        if (index >= objs_prefabs.Count)
        {
            Debug.Log("index out of range: "+ index +" max:" + objs_prefabs.Count);
            return;

        }
        if (Global.state == Global.ADD_OBJ)
        {
            born_pos = Vector3.zero;
            born_rot = new Quaternion(0, 0, 0, 0);
            GameObject newObject = Instantiate(objs_prefabs[index], born_pos, born_rot) as GameObject;
            HandCtrl.DisChoose();
            HandCtrl.ChooseObject(newObject);

            //然后要选择位置
            GetComponent<UICtrl>().UICtrlMsgSetString("请选择移动的位置");
            OnObjLibExit();
            Global.state = Global.MOVE_OBJ;

        }
        else if (Global.state == Global.REPLACE_OBJ)
        {
            if (Global.chosenObj == null)
            {
                GetComponent<UICtrl>().UICtrlMsgSetString("请先选择物体！");
                return;
            }
            //用加载得到的资源对象，实例化游戏对象，实现游戏物体的动态加载
            born_pos = Global.chosenObj.transform.position;
            born_rot = Global.chosenObj.transform.rotation;
            GameObject newObject = Instantiate(objs_prefabs[index], born_pos, born_rot) as GameObject;
            Global.chosenObj.GetComponent<ObjShowRim>().OnSelectExit();
            Global.chosenObj.SetActive(false);
            Global.chosenObj = null;
            HandCtrl.ChooseObject(newObject);
            //关闭library页面 回到idle状态
            OnObjLibExit();
            Global.state = Global.IDLE;
        }

    }

    void MatBtnOnClick(int index)
    {
        if (Global.state == Global.CHANGE_MATERIAL)
        {
            if (Global.chosenObj == null)
            {
                GetComponent<UICtrl>().UICtrlMsgSetString("请先选择物体！");
                return;
            }

            //TODO handle click and instatiate
        }

    }

    public void ReturnToMenuOnClick(int mode)
    {
        if (mode == 0) OnObjLibExit();
        else if (mode == 1) OnMatLibExit();

        Global.state = Global.WATCH_MENU;
        this.GetComponent<UIMenu>().OnMenuEnter();
    }

    void CloseOnClick(int mode)
    {
        Global.state = Global.IDLE;
        if (mode == 0) OnObjLibExit();
        else if (mode == 1) OnMatLibExit();

    }


    public void OnObjLibEnter()
    {
        objs_lib_ui.SetActive(true);
    }
    public void OnObjLibExit()
    {
        objs_lib_ui.SetActive(false);
    }
    public void OnMatLibEnter()
    {
        mats_lib_ui.SetActive(true);
    }

    public void OnMatLibExit()
    {
        mats_lib_ui.SetActive(false);

    }

}
